define(() =>
/**
 * Instances of this class are thrown when there is a syntax error in the NetCDF file being parsed.
 */
class NetCdfSyntaxError extends Error {

constructor(msg)
{
  super(msg);
}

} );
