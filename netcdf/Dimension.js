define(() =>
/**
 * Class representing a NetCDF dimension.
 */
class NetCdfDimension {

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance with the information given in a NetCDF Header.
 */
constructor(name, numValues)
{
  this._name = name;
  this._numValues = numValues;
}

//------------------------------------------------------------------------------------------------//

/** @return {string} The name of this dimension. */
get name() { return this._name; }

/** @return {number} Number of values in this dimension. */
get numValues() { return this._numValues; }

} );
