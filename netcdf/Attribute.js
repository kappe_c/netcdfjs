define(['netcdf/TypedData'], (NetCdfTypedData) =>
/**
 * Class representing a NetCDF attribute.
 */
class NetCdfAttribute extends NetCdfTypedData {

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance holding a string.
 */
static fromString(name, string)
{
  return new NetCdfAttribute(name, 2, NetCdfTypedData.string2utf8Array(string), string);
}

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance with the information given in a NetCDF Header.
 * @param {string} [strVal] The data as string.
 */
constructor(name, typeTag, data, strVal)
{
  super(name, typeTag);
  this._data = data;
  if(typeTag === 2)
    this._string = strVal || NetCdfTypedData.utf8Array2string(data);
}

//------------------------------------------------------------------------------------------------//

/** @return {Array|TypedArray} List of values. */
get data() { return this._data; }

/** @return {undefined|string} Concatenation of the characters in case the type is "Uint8". */
get string() { return this._string; }

} );
