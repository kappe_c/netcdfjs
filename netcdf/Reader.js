define(['netcdf/Attribute',
        'netcdf/Dimension',
        'netcdf/File',
        'netcdf/SyntaxError',
        'netcdf/TypedData',
        'netcdf/Variable'],
        (NetCdfAttribute,
         NetCdfDimension,
         NetCdfFile,
         NetCdfSyntaxError,
         NetCdfTypedData,
         NetCdfVariable) =>
/**
 * Class containing methods to read a NetCDF file.
 */
class NetCdfReader {

/**
 * Convenience function to directly read a file in one go.
 * Parameters: see `constructor`. Return value: see `read`.
 */
static read(arrBuf, fileName, wantVerbose)
{
  return (new NetCdfReader(arrBuf, fileName, wantVerbose)).read();
}

//------------------------------------------------------------------------------------------------//

/**
 * Object to read the content of a NetCDF file from an array buffer.
 * @param {ArrayBuffer} arrBuf E.g. as read with `myFileReader.readAsArrayBuffer`.
 * @param {string} fileName E.g. as returned from `myFile.name`.
 * @param {boolean} wantVerbose Log the variable names and static data?
 */
constructor(arrBuf, fileName, wantVerbose = false)
{
  this._ab = arrBuf;
  this._dv = new DataView(arrBuf);
  this._off = 0; // offset pointing to the next unread byte
  this._fileName = fileName;
  this._wantVerbose = wantVerbose;
  
  this._typeTag2readFun =
  [
    undefined,
    () => this._dv.getInt8( this._off++),
    () => this._dv.getUint8(this._off++),
    () => { const res = this._dv.getInt16(this._off); this._off += 2; return res; },
    () => { const res = this._dv.getInt32(this._off); this._off += 4; return res; },
    () => { const res = this._dv.getFloat32(this._off); this._off += 4; return res; },
    () => { const res = this._dv.getFloat64(this._off); this._off += 8; return res; }
  ];
  
  const tagDim = 10, tagVar = 11, tagAtt = 12;
  
  this._listTag2readFun = [];
  this._listTag2readFun[tagDim] = name => this.readDim(name);
  this._listTag2readFun[tagVar] = name => this.readVar(name);
  this._listTag2readFun[tagAtt] = name => this.readAtt(name);
  
  this.readDims = this.getReadListFun(tagDim);
  this.readVars = this.getReadListFun(tagVar);
  this.readAtts = this.getReadListFun(tagAtt);
}

//------------------------------------------------------------------------------------------------//

/**
 * @public
 * @return {NetCdfFile} The completely read file.
 * @throw NetCdfSyntaxError
 */
read()
{
  this.readHeader();
  this.readData();
  return this._file;
}

//------------------------------------------------------------------------------------------------//

/**
 * @public
 * @return {NetCdfFile} The data part is not yet loaded.
 * @throw NetCdfSyntaxError
 */
readHeader()
{
  const cc2str = String.fromCharCode;
  const magic012 =
    cc2str(this._dv.getUint8(this._off++)) +
    cc2str(this._dv.getUint8(this._off++)) +
    cc2str(this._dv.getUint8(this._off++));
  const magic3Raw = this._dv.getUint8(this._off++);
  if(magic012 !== 'CDF')
    throw new NetCdfSyntaxError('Only the NetCDF classic format is supported but we found a "magic number" reading "' + magic012 + cc2str(magic3Raw) + '"');
  if(magic3Raw === 2)
    throw new NetCdfSyntaxError('The 64-bit offset format is not supported');
  if(magic3Raw !== 1)
    throw new NetCdfSyntaxError('Unknown version number "' + magic3Raw + '" (must be "1")');
  
  const numRecsEntry = this.readNonNeg();
  this._numRecords = numRecsEntry === Math.pow(2, 32) - 1 ? undefined : numRecsEntry;
  
  this._dims = this.readDims();
  
  // Get the record dimension (number of values is 0) and make sure that there is at most one.
  this._recordDimIdx = null;
  const numDims = this._dims.length;
  for(let i = 0; i < numDims; ++i) {
    if(this._dims[i].numValues === 0) {
      if(this._recordDimIdx === null)
        this._recordDimIdx = i;
      else
        throw new NetCdfSyntaxError('The number of values in a dimension must be greater than zero, except for the unique record dimension');
    }
  }
  
  const atts = this.readAtts();
  const vars = this.readVars();
  
  return this._file = new NetCdfFile(this._fileName, this._numRecords, this._dims, atts, vars);
}

//------------------------------------------------------------------------------------------------//

getReadListFun(tag)
{
  const read = this._listTag2readFun[tag];
  return () =>
  {
    const listTag = this.readNonNeg();
    if(listTag === 0)
    {
      const zero  = this.readNonNeg();
      this.assert(zero, 0);
      return [];
    }
    this.assert(listTag, tag);
    const n = this.readNonNeg();
    const arr = new Array(n);
    for(let i = 0; i < n; ++i)
      arr[i] = read(this.readName());
    return arr;
  };
}

//------------------------------------------------------------------------------------------------//

readDim(name)
{
  return new NetCdfDimension(name, this.readNonNeg());
}

//------------------------------------------------------------------------------------------------//

readVar(name)
{
  const numDims = this.readNonNeg(); // dimensionality of the domain of this variable
  const dimIndices = new Array(numDims);
  for(let i = 0; i < numDims; ++i)
    dimIndices[i] = this.readNonNeg();
  const atts = this.readAtts();
  const typeTag = this.readTypeTag();
  
  const numBytesPerRecord = this.readNonNeg(); // total number if not a record variable
  const beginByteOffset = this.readNonNeg(); // file seek index
  
  // If this variable depends on the record dimension, the record dimension index must be
  // the first element in `dimIndices`.
  // `dimIndices[0]` may be `undefined`, `this._recordDimIdx` may be `null`.
  const isRecordVar = dimIndices[0] === this._recordDimIdx;
  for(let i = 1; i < numDims; ++i)
    if(dimIndices[i] === this._recordDimIdx)
      throw new NetCdfSyntaxError('The record dimension must be the first one in the list of  dimensions of a variable (it is number ' + (i+1) + ' for the variable "' + name + '")');
  
  // The `numBytesPerRecord` is actually redundant and must match the value one can compute.
  const numBytesPerValue = NetCdfTypedData.typeTag2numBytes[typeTag];
  const numValuesPerRecord = // total number if not a record variable
    dimIndices.map(i => this._dims[i].numValues || 1).reduce((acc, val) => acc * val, 1);
  const computedValue = numBytesPerValue * numValuesPerRecord;
  this.assert(numBytesPerRecord, computedValue + computedValue % 4);
  
  const vbl = new NetCdfVariable(name, typeTag, dimIndices, numValuesPerRecord, isRecordVar, atts);
  // Store the `_beginByteOffset` temporarily for an assertion when the data is read.
  vbl._beginByteOffset = beginByteOffset;
  return vbl;
}

//------------------------------------------------------------------------------------------------//

readAtt(name)
{
  const typeTag = this.readNonNeg();
  if(typeTag === 0 || typeTag > 6)
    throw new NetCdfSyntaxError('Invalid type tag: ' + typeTag);
  const read = this._typeTag2readFun[typeTag];
  const n = this.readNonNeg();
  const data = new Array(n);
  for(let i = 0; i < n; ++i)
    data[i] = read();
  if(typeTag < 4) // potentially padding for byte, char, short
    this.skipPadding();
  return new NetCdfAttribute(name, typeTag, data);
}

//------------------------------------------------------------------------------------------------//

readName()
{
  const n = this.readNonNeg();
  const arr = new Array(n);
  for(let i = 0; i < n; ++i)
    arr[i] = String.fromCharCode(this._dv.getUint8(this._off++));
  this.skipPadding();
  return arr.join('');
}


readNonNeg()
{
  const res = this._dv.getUint32(this._off);
  this._off += 4;
  return res;
}


readTypeTag()
{
  const res = this.readNonNeg();
  if(NetCdfTypedData.typeTag2numBytes[res] === undefined)
    throw new NetCdfSyntaxError('Invalid type tag: ' + res);
  return res;
}


skipPadding()
{
  const remainder = this._off % 4;
  if(remainder !== 0)
    this._off += 4 - remainder;
}


assert(data, hope)
{
  if(data !== hope)
    throw new NetCdfSyntaxError('Expected ' + hope + ', got ' + data);
}

//------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------//

/**
 * @public
 * Read the data part of the file, completing the already existing variables.
 * May only be called after `readHeader`.
 */
readData()
{
  this.readStaticData();
  this.readRecords();
}


/**
 * @public
 * Read the static data part of the file, completing the non-record variables.
 * May only be called after `readHeader`.
 */
readStaticData()
{
  if(this._wantVerbose === true)
    console.log('static variables:');
  for(let vbl of this._file.staticVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    if(vbl !== this._file.staticVars[0] || this._off > vbl._beginByteOffset)
      this.assert(this._off, vbl._beginByteOffset);
    else if(this._off < vbl._beginByteOffset) {
      if(this._wantVerbose === true)
        console.log('There is extra padding between the header and the data:',
                    vbl._beginByteOffset - this._off, 'bytes');
      this._off = vbl._beginByteOffset; // skip the random data
    }
    delete vbl._beginByteOffset;
    const read = this._typeTag2readFun[vbl.typeTag];
    const numValues = vbl._numValuesPerRecord;
    const data = vbl._data = new Array(numValues);
    for(let i = 0; i < numValues; ++i)
      data[i] = read();
    if(this._wantVerbose === true)
      console.log(data);
    if(vbl.typeTag < 4) // potentially padding for byte, char, short
      this.skipPadding();
  }
}


/**
 * @public
 * Read the data records from the file, completing the record variables.
 * May only be called after `readStaticData`.
 */
readRecords()
{
  const numRecs = this._numRecords;
  if(numRecs === 0)
    return
  if(numRecs === undefined) { // We currently don't handle streaming data.
    console.error('The number of records in this file is undefined (set to "streaming")')
    return
  }
  
  const isLittleEndian = new Uint8Array(new Uint32Array([0x12345678]).buffer)[0] === 0x78;
//   console.log('The client processor is', isLittleEndian ? 'little' : 'big', 'endian');
  const swapBytes = NetCdfTypedData.swapBytes;
  const recVars = this._file.recordVars;
  
  const recordSize = recVars.length !== 1 ?
    recVars.reduce((acc, vbl) => acc + vbl.numBytesPerRecord, 0) :
    recVars[0]._numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[recVars[0].typeTag];
  
  // First, define the individual functions to read one record.
  if(this._wantVerbose === true && recVars.length > 0)
    console.log('record variables:');
  for(let vbl of recVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    const records = vbl._records = new Array(numRecs);
    
    const myBeg = vbl._beginByteOffset; delete vbl._beginByteOffset;
    const MyArr = NetCdfTypedData.typeTag2arrCtor[vbl.typeTag];
    const numValuesPerRecord = vbl._numValuesPerRecord;
    const numBytesPerValue = NetCdfTypedData.typeTag2numBytes[vbl.typeTag];
    const numTrueBytesPerRecord = recVars.length !== 1 ? vbl.numBytesPerRecord : recordSize;
    
    const assert = rec => this.assert(this._off, myBeg + recordSize * rec);
    
    if(vbl.kind !== 'data')
    {
      const read = this._typeTag2readFun[vbl.typeTag];
      vbl.readRecord = rec =>
      {
        assert(rec);
        const data = records[rec] = new Array(numValuesPerRecord);
        for(let i = 0; i < numValuesPerRecord; ++i)
          data[i] = read();
        if(vbl.typeTag < 4)
          this.skipPadding();
      };
      continue;
    }
    
    const readBigEndian = rec =>
    {
      assert(rec);
      // NOTE This approach does not work for Float64 data if this._off is not a multiple of 8.
      records[rec] = new MyArr(this._ab, this._off, numValuesPerRecord);
      this._off += numTrueBytesPerRecord;
    };
    vbl.readRecord = (isLittleEndian===false || numBytesPerValue===1) ? readBigEndian : rec =>
    {
      swapBytes(new Uint8Array(this._ab, this._off, numTrueBytesPerRecord), numBytesPerValue);
      readBigEndian(rec);
    };
  }
  
  const recMax = numRecs - 1;
  for(let rec = 0; rec < numRecs; ++rec)
  {
    if(this._wantVerbose === true && rec % 24 === 0)
      console.log('reading records', rec, 'through', Math.min(recMax, rec + 23));
    for(let vbl of recVars)
      vbl.readRecord(rec);
  }
}

})
