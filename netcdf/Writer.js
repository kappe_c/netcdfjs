define(['netcdf/TypedData'], (NetCdfTypedData) =>
/**
 * Class containing methods to write a NetCDF file.
 */
class NetCdfWriter {

/**
 * Convenience function to directly write a file in one go.
 * Parameters: see `constructor`. Return value: see `write`.
 */
static write(file, wantVerbose)
{
  return (new NetCdfWriter(file, wantVerbose)).write();
}

//------------------------------------------------------------------------------------------------//

/**
 * Object to write the content of a NetCDF file to an array buffer.
 * @param {NetCdfFile} file File to write.
 * @param {boolean} wantVerbose Log the variable names and static data?
 */
constructor(file, wantVerbose = false)
{
  this._ab = null;
  this._dv = null;
  this._off = 0; // offset pointing to the next byte to write
  this._file = file;
  this._wantVerbose = wantVerbose;
  
  this._typeTag2writeFun =
  [
    undefined,
    x => this._dv.setInt8( this._off++, x),
    x => this._dv.setUint8(this._off++, x),
    x => { this._dv.setInt16(  this._off, x); this._off += 2; },
    x => { this._dv.setInt32(  this._off, x); this._off += 4; },
    x => { this._dv.setFloat32(this._off, x); this._off += 4; },
    x => { this._dv.setFloat64(this._off, x); this._off += 8; }
  ];
  
  const tagDim = 10, tagVar = 11, tagAtt = 12;
  
  this._listTag2writeFun = [];
  this._listTag2writeFun[tagDim] = dim => this.writeDim(dim);
  this._listTag2writeFun[tagVar] = vbl => this.writeVar(vbl);
  this._listTag2writeFun[tagAtt] = att => this.writeAtt(att);
  
  this.writeDims = this.getWriteListFun(tagDim);
  this.writeVars = this.getWriteListFun(tagVar);
  this.writeAtts = this.getWriteListFun(tagAtt);
}

//------------------------------------------------------------------------------------------------//

/**
 * @public
 * It is currently only possible to write the file all at once, with this method.
 * @return {ArrayBuffer} The complete file written to an array buffer.
 */
write()
{
  this._ab = new ArrayBuffer(this.compFileSize());
  this._dv = new DataView(this._ab);
  this.writeHeader();
  this.writeData();
  return this._ab;
}

//------------------------------------------------------------------------------------------------//

writeHeader()
{
  this._dv.setUint8(this._off++, 67); // "C"
  this._dv.setUint8(this._off++, 68); // "D"
  this._dv.setUint8(this._off++, 70); // "F"
  this._dv.setUint8(this._off++, 1);
  const numRecs = this._file.numRecords;
  this.writeNonNeg(numRecs !== undefined ? numRecs : Math.pow(2, 32) - 1);
  this.compBeginByteOffset();
  this.writeDims(this._file.dims);
  this.writeAtts(this._file.atts);
  this.writeVars(this._file.vars);
}

compBeginByteOffset() // Is deleted after the data is written.
{
  let nxt = this._beginData;
  for(let vbl of this._file.staticVars) {
    vbl._beginByteOffset = nxt;
    nxt += vbl.numBytesPerRecord;
  }
  // The code may be the same for both kinds of variables but we cannot be sure that the static
  // variables come first in `this._file.vars`.
  for(let vbl of this._file.recordVars) {
    vbl._beginByteOffset = nxt;
    nxt += vbl.numBytesPerRecord;
  }
}

getWriteListFun(listTag)
{
  const write = this._listTag2writeFun[listTag];
  return arr =>
  {
    if(arr.length === 0)
    {
      this.writeNonNeg(0);
      this.writeNonNeg(0);
      return;
    }
    this.writeNonNeg(listTag);
    this.writeNonNeg(arr.length);
    for(let obj of arr)
    {
      this.writeNonNeg(obj._utf8arr.length);
      obj._utf8arr.forEach(x => this._dv.setUint8(this._off++, x));
      delete obj._utf8arr;
      this.addPadding();
      write(obj);
    }
  }
}

writeDim(dim)
{
  this.writeNonNeg(dim.numValues);
}

writeVar(vbl)
{
  this.writeNonNeg(vbl.dimIndices.length);
  vbl.dimIndices.forEach(this.writeNonNeg, this);
  this.writeAtts(vbl.atts);
  this.writeNonNeg(vbl.typeTag);
  this.writeNonNeg(vbl.numBytesPerRecord);
  this.writeNonNeg(vbl._beginByteOffset);
}

writeAtt(att)
{
  const typeTag = att.typeTag;
  this.writeNonNeg(typeTag);
  const numValues = att.data.length;
  this.writeNonNeg(numValues);
  att.data.forEach(this._typeTag2writeFun[typeTag]);
  this.addPadding();
}

writeNonNeg(x)
{
//   console.assert(x >= 0, 'Expected a non-negative integer, got ' + x);
  this._dv.setUint32(this._off, x);
  this._off += 4;
}

addPadding()
{
  const remainder = this._off % 4;
  // JS array buffers are initialized to 0, so we honor the specification for the header padding.
  if(remainder !== 0)
    this._off += 4 - remainder;
}

//------------------------------------------------------------------------------------------------//

// According to the specification, any padding should be filled with the fill value of the
// respective variable. We do not do this currently because this memory is ignored anyway.
/**
 * Write the data part of the file. May only be called after `writeHeader`.
 */
writeData()
{
  this.writeStaticData();
  this.writeRecords();
}


/**
 * Write the static data. May only be called after `writeHeader`.
 */
writeStaticData()
{
  if(this._wantVerbose === true)
    console.log('static variables:');
  for(let vbl of this._file.staticVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    console.assert(this._off === vbl._beginByteOffset,
                   'writer offset: ' + this._off + '  variable offset: ' + vbl._beginByteOffset);
    delete vbl._beginByteOffset;
    vbl.data.forEach(this._typeTag2writeFun[vbl.typeTag]);
    this.addPadding();
  }
}


/**
 * Write the data records. May only be called after `writeStaticData`.
 */
writeRecords()
{
  const numRecs = this._file.numRecords;
  if(numRecs === 0)
    return
  if(numRecs === undefined) { // We currently don't handle streaming data.
    console.error('The number of records in this file is undefined (set to "streaming")')
    return
  }
  
  const isLittleEndian = new Uint8Array(new Uint32Array([0x12345678]).buffer)[0] === 0x78;
//   console.log('The client processor is', isLittleEndian ? 'little' : 'big', 'endian');
  const swapBytes = NetCdfTypedData.swapBytes;
  const recVars = this._file.recordVars;
  
  const recordSize = recVars.length !== 1 ?
    recVars.reduce((acc, vbl) => acc + vbl.numBytesPerRecord, 0) :
    recVars[0]._numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[recVars[0].typeTag];
  
  // First, define the individual functions to write one record.
  if(this._wantVerbose === true && recVars.length > 0)
    console.log('record variables:');
  for(let vbl of recVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    const records = vbl._records;
    
    const myBeg = vbl._beginByteOffset; delete vbl._beginByteOffset;
    const MyArr = NetCdfTypedData.typeTag2arrCtor[vbl.typeTag];
    const numValuesPerRecord = vbl._numValuesPerRecord;
    const numBytesPerValue = NetCdfTypedData.typeTag2numBytes[vbl.typeTag];
    const numTrueBytesPerRecord = recVars.length !== 1 ? vbl.numBytesPerRecord : recordSize;
    const assert = rec => console.assert(
      this._off === myBeg + recordSize * rec,
      'writer offset: ' + this._off + '  ' +
      'computed offset: ' + (myBeg + recordSize * rec));
    
    if(records[0] instanceof Array)//if(vbl.kind !== 'data') // The records are not typed arrays
    {
      const write = this._typeTag2writeFun[vbl.typeTag];
      vbl.writeRecord = rec =>
      {
        assert(rec);
        records[rec].forEach(write);
        this.addPadding();
      };
      continue;
    }
    
    const writeBigEndian = rec =>
    {
      assert(rec);
      // NOTE This approach does not work for Float64 data if this._off is not a multiple of 8.
      // temporary variable for readability
      const arrViewOnBuf = new MyArr(this._ab, this._off, numValuesPerRecord);
      arrViewOnBuf.set(records[rec]);
      this._off += numTrueBytesPerRecord; // padding with zeros
    };
    vbl.writeRecord = (isLittleEndian===false || numBytesPerValue===1) ? writeBigEndian : rec =>
    {
      const off = this._off;
      writeBigEndian(rec);
      swapBytes(new Uint8Array(this._ab, off, numTrueBytesPerRecord), numBytesPerValue);
    };
  }
    
  for(let rec = 0; rec < numRecs; ++rec)
  {
    if(this._wantVerbose === true)
      console.log('record', rec);
    for(let vbl of recVars)
      vbl.writeRecord(rec);
  }
}

//------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------//

compFileSize()
{
  this._beginData = 4 + 4 + // magic + numRecords
    this.compDimListSize(this._file.dims) +
    this.compAttListSize(this._file.atts) +
    this.compVarListSize(this._file.vars);
  return this._beginData + this.compDataSize(this._file.vars);
}

compDimListSize(dims)
{
  let res = 8;
  for(let dim of dims)
    res += this.compNameSize(dim) + 4; // name + numValues
  return res;
}

compAttListSize(atts)
{
  let res = 8;
  for(let att of atts) {
    const numDataBytes = att.data.length * NetCdfTypedData.typeTag2numBytes[att.typeTag];
    const remainder = numDataBytes % 4;
    const padding = remainder === 0 ? 0 : 4 - remainder;
    // name + typeTag + numValues + data
    res += this.compNameSize(att) + 4 + 4 + numDataBytes + padding;
  }
  return res;
}

compVarListSize(vars)
{
  let res = 8;
  for(let vbl of vars)
    // name + numDims + dimIndices + atts + typeTag + numBytesPerRecord + beginByteOffset
    res += this.compNameSize(vbl) + 4 + 4*vbl.dimIndices.length +
           this.compAttListSize(vbl.atts) + 4*3;
  return res;
}

compNameSize(obj)
{
  obj._utf8arr = NetCdfTypedData.string2utf8Array(obj.name);
  let res = obj._utf8arr.length + 4; // name + number of UTF-8 codes
  const remainder = res % 4;
  if(remainder !== 0)
    res += 4 - remainder;
  return res;
}

compDataSize(vars)
{
  let res = 0;
  for(let vbl of this._file.staticVars)
   res += vbl.numBytesPerRecord;
  
  const recVars = this._file.recordVars;
  const numRecs = this._file.numRecords;
  if(recVars.length !== 1)
    return recVars.reduce((acc, vbl) => acc + numRecs * vbl.numBytesPerRecord, res);
  
  const vbl = recVars[0];
  return res + numRecs * vbl.numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[vbl.typeTag];
}

} );
