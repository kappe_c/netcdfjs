define(() => {

const klass =
/**
 * Class representing named and typed data in netCDF format, that also has some noteworthy
 * static members.
 */
class NetCdfTypedData {

//------------------------------------------------------------------------------------------------//

/**
 * See the respective getters for a description of the parameters.
 */
constructor(name, typeTag)
{
  this._name = name;
  this._typeTag = typeTag;
}

//------------------------------------------------------------------------------------------------//

/** @return {string} Name of this data. */
get name() { return this._name; }

/** @return {number} Integer in [1,6]. */
get typeTag() { return this._typeTag; }

/** @return {string} One of "Int8", "Uint8", "Int16", "Int32", "Float32", "Float64". */
get type() { return klass.typeTag2str[this._typeTag]; }

//------------------------------------------------------------------------------------------------//

/**
 * @private
 * Swap the bytes of the given `Uint8Array` in chunks of `typeSize`.
 */
static swapBytes(u8Arr, typeSize)
{
  const n = u8Arr.length;
  let tmp;
  if(typeSize === 4) // most likely case first: Float32 or Int32
  {
    for(let i = 0; i < n; i += 2) {
      tmp = u8Arr[i];
      u8Arr[i] = u8Arr[i+3];
      u8Arr[i+3] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[++i];
      u8Arr[i] = tmp;
    }
  }
  else if(typeSize === 8)
  {
    for(let i = 0; i < n; i += 4) {
      tmp = u8Arr[i];
      u8Arr[i] = u8Arr[i+7];
      u8Arr[i+7] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[i+5];
      u8Arr[i+5] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[i+3];
      u8Arr[i+3] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[++i];
      u8Arr[i] = tmp;
    }
  }
  else// if(typeSize === 2)
  {
    for(let i = 0; i < n; ++i) {
      tmp = u8Arr[i];
      u8Arr[i] = u8Arr[++i];
      u8Arr[i] = tmp;
    }
  }
}

//------------------------------------------------------------------------------------------------//

/** Convert a string (UTF-16) to an array of UTF-8 character codes. */
static string2utf8Array(str)
{
  const res = [];
  const numCodes = str.length;
  for(let i = 0; i < numCodes; ++i)
  {
    const code16 = str.charCodeAt(i);
    if(code16 < 0x80)
      res.push(code16);
    else if(code16 < 0x800)
      res.push(0xc0 | (code16 >> 6),
               0x80 | (code16 & 0x3f));
    else if(code16 < 0xd800 || code16 >= 0xe000)
      res.push(0xe0 | ( code16 >> 12),
               0x80 | ((code16 >> 6) & 0x3f),
               0x80 | ( code16       & 0x3f));
    else { // surrogate pair
      const code32 = ((code16 & 0x3ff) << 10) | (str.code16At(++i) & 0x3ff)
      res.push(0xf0 |  (code32 >> 18),
               0x80 | ((code32 >> 12) & 0x3f),
               0x80 | ((code32 >>  6) & 0x3f),
               0x80 |  (code32 & 0x3f));
    }
  }
  return res;
}

//------------------------------------------------------------------------------------------------//

/** Convert an array of UTF-8 character codes to a string (UTF-16). */
static utf8Array2string(arr)
{
  let tmp = '';
  for(let code of arr) {
    if(code === 0)
      break;
    tmp += '%' + ('0' + code.toString(16)).slice(-2);
  }
  return decodeURIComponent(tmp);
}

} // END class


// Static data members. We assume that *char* is an *Uint8*.
klass.typeTag2str = [, 'Int8', 'Uint8', 'Int16', 'Int32', 'Float32', 'Float64'];
klass.typeTag2numBytes = [, 1, 1, 2, 4, 4, 8];
klass.typeTag2arrCtor = [, Int8Array, Uint8Array, Int16Array, Int32Array, Float32Array, Float64Array];
klass.str2typeTag = { Int8: 1, Uint8: 2, Int16: 3, Int32: 4, Float32: 5, Float64: 6 };

return klass;

});
