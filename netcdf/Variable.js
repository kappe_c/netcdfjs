define(['netcdf/TypedData'], (NetCdfTypedData) =>
/**
 * Class representing a NetCDF variable.
 */
class NetCdfVariable extends NetCdfTypedData {

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance with the information given in a NetCDF Header.
 * @param {number} numValuesPerRecord Total number if not a record variable.
 */
constructor(name = 'scalar_var', typeTag = 6, dimIndices = [], numValuesPerRecord = 1,
            isRecordVar = false, atts = [], datOrRecs)
{
  super(name, typeTag);
  this._dimIndices = dimIndices;
  this._atts = atts;
  this._isRecordVar = isRecordVar;
  this._numValuesPerRecord = numValuesPerRecord;
  if(datOrRecs !== undefined)
    this[isRecordVar === false ? '_data' : '_records'] = datOrRecs;
  if(atts.find(att => att.name === '_Unsigned' && att.string === 'true') !== undefined)
    console.warn('The variable "' + name + '" has the attribute "_Unsigned" set to "true"; this is not treated in any special way, however, by netcdfjs')
}

//------------------------------------------------------------------------------------------------//

/** @return {Array} List of dimensions (by zero-based index) that the variable depends on. */
get dimIndices() { return this._dimIndices; }

/** @return {Array} List of attributes. */
get atts() { return this._atts; }

/** @return {boolean} Is it a record variable (one that depends on the record dimension)? */
get isRecordVar() { return this._isRecordVar; }

/**
 * What kind of variable is this?
 * One of "data", "coordinate", "auxiliary", "boundary", "measure", "count".
 * Determined (and further documented) in `NetCdfFile.determineVarKind`.
 */
get kind() { return this._kind; }

/** @return {number} Number of values per record. */
get numValuesPerRecord() { return this._numValuesPerRecord; }

/** @return {number} Number of bytes per record, including padding. */
get numBytesPerRecord()
{
  const n = this._numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[this._typeTag];
  return n + (n % 4);
}

/**
 * @return {Array|TypedArray} Array of values if this is not a record variable; otherwise
 * the records are merged into a single (typed) array (again for each access of the getter).
 */
get data()
{
  if(this._data !== undefined)
    return this._data;
  if(this._kind !== 'data')
    return this._records.reduce((acc, rec) => { acc.push(...rec); return acc; }, []);
  const nPerRec = this._numValuesPerRecord;
  const res = new NetCdfTypedData.typeTag2arrCtor[this._typeTag](nPerRec * this._records.length);
  this._records.forEach((val, idx) => res.set(val, nPerRec * idx));
  return res;
}

/**
 * @return {undefined|Array[Array|TypedArray]} Array of values per record if this is a record 
 * variable. If this is a *data* variable, the values are stored in `TypedArray`s.
 */
get records() { return this._records; }

} );
