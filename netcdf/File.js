define(() =>
/**
 * Class representing a NetCDF file.
 */
class NetCdfFile {

/**
 * Construct an instance with the information given in a NetCDF Header.
 * The `data` or `records` of the variables need not yet be defined.
 * @param numRecords See respective getter.
 */
constructor(name, numRecords, dims, atts, vars)
{
  this._name = name;
  this._numRecords = numRecords;
  this._dims = dims;
  this._atts = atts;
  this._vars = vars;
  
  this._staticVars = [];
  this._recordVars = [];
  for(let vbl of vars)
   (vbl.isRecordVar !== true ? this._staticVars : this._recordVars).push(vbl);
   
  this.determineVarKind();
}

//------------------------------------------------------------------------------------------------//

/** @return {string} The name of this file (including ".nc"). */
get name() { return this._name; }

/**
 * @return {number|undefined|} Undefined if indeterminate (record dimension is unlimited).
 * 0 if there is no record dimension.
 */
get numRecords() { return this._numRecords; }

/** @return {Array} List of axes that specify the domain of the variables. */
get dims() { return this._dims; }

/** @return {Array} List of global attributes. */
get atts() { return this._atts; }

/** @return {Array} List of variables. */
get vars() { return this._vars; }


/** @return {Array} Variables (subset of `vars`) that do not depend on the record dimension. */
get staticVars() { return this._staticVars; }

/** @return {Array} Variables (subset of `vars`) that depend on the record dimension. */
get recordVars() { return this._recordVars; }


/**
 * Get an object that maps the kind of a variable (as string) to an array of the respective
 * variables. See `determineVarKind` for details.
 */
get kind2vars() { return this._kind2vars; }

//------------------------------------------------------------------------------------------------//

/**
 * Classify the variables as *data*, *coordinate*, *auxiliary coordinate*, *boundary*, *measure*
 * and *count* as standardized in
 * [CF (Climate and Forecast) Conventions and Metadata](http://cfconventions.org/).
 * See the netcdfjs readme (and or the comments in the code below) for a little more information.
 * @private Typically only called once in constructor.
 */
determineVarKind()
{
  const kind2vars = this._kind2vars =
  {
    data: [],
    coordinate: [],
    auxiliary: [],
    boundary: [],
    measure: [],
    count: []
  };
  
  const classified = new Set();
  
  const add = (vbl, kind) =>
  {
    console.assert(classified.has(vbl) === false, 'Variable "' + vbl.name + '" cannot be classified uniquely (is both "' + vbl.kind + '" and "' + kind + '")');
    kind2vars[kind].push(vbl);
    classified.add(vbl);
    vbl._kind = kind;
  };
  
  const findAndAdd = (vblName, kind) =>
  {
    const vbl = this._vars.find(vbl => vbl.name === vblName);
    console.assert(vbl !== undefined, 'Cannot find variable "' + vblName + '"');
    add(vbl, kind);
  };
  
  const extVar2flag = {};
  {
    const extVars = this._atts.find(a => a.name === 'external_variables');
    if(extVars !== undefined)
      extVars.string.split(' ').forEach(str => extVar2flag[str] = true);
  }
  
  const pairRegExp = /([^ ]+?): ([^ ]+)/g;
  
  // Firstly, classify by explicit attributes.
  for(let vbl of this._vars)
  {
    for(let att of vbl.atts)
    {
      switch(att.name)
      {
        // In the following cases another variable is classified.
        case 'coordinates':
          // The *coordinates* attribute is used on a data variable to unambiguously identify the
          // relevant space and time *auxiliary coordinate* variables.
          // The value of the coordinates attribute is a blank separated list of the names of
          // *auxiliary coordinate* variables.
          att.string.split(' ').forEach(vblName => findAndAdd(vblName, 'auxiliary'));
          break;
        case 'bounds':
          // *coordinate* variables may have a *bounds* attribute whose value is the name of a
          // *boundary* variable.
          findAndAdd(att.string, 'boundary');
          break;
        case 'cell_measures':
          // See cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#cell-measures
          // This is a string attribute comprising a list of blank-separated pairs of words of the
          // form "measure: name".
          // For the moment, "area" and "volume" are the only defined measures, but others may be
          // supported in future.
          // The "name" is the name of the variable containing the measure values.
          const cmStr = att.string;
          pairRegExp.lastIndex = 0;
          for(let res = pairRegExp.exec(cmStr); res !== null; res = pairRegExp.exec(cmStr))
          {
            const measure = res[1], mvblName = res[2];
            // The variable is not required to be present in the file. If the variable is located
            // in another file, it must be listed in the *external_variables* attribute of the file.
            const mvbl = this._vars.find(v => v.name === mvblName);
            if(mvbl !== undefined)
              add(mvbl, 'measure');
            else {
              if(extVar2flag[mvblName] !== true)
                console.warn(`The external variable "${mvblName}" is not listed under the global attribute *external_variables*`);
              let fileInfo = '';
              const assoFiles = vbl._atts.find(a => a.name === 'associated_files');
              if(assoFiles !== undefined)
              {
                let base = '', url;
                const afStr = assoFiles.string;
                const idxBak = pairRegExp.lastIndex;
                pairRegExp.lastIndex = 0;
                for(let res = pairRegExp.exec(afStr); res !== null; res = pairRegExp.exec(afStr))
                {
                  const key = res[1], val = res[2];
                  if(key === 'baseURL')
                    base = val;
                  else if(key === mvblName) {
                    url = val;
                    break;
                  }
                }
                pairRegExp.lastIndex = idxBak;
                if(url !== undefined)
                  fileInfo = ` (${base}/${url})`;
              }
              console.info(`The data for the cell measure *${measure}* for the variable "${vbl.name}" is in an external file${fileInfo} as variable "${mvblName}"`);
            }
          }
          break;
        // This classifies the current variable itself.
        case 'sample_dimension':
          // The value of this attribute is the name of the dimension it counts.
          console.assert(this._dims.find(dim => dim.name === att.string) !== undefined, 'The value of the attribute "sample_dimension" (of variable "' + vbl.name + '") must be the name of a dimension but it is "' + att.string + '"');
          // This variable must be of an integer type.
          console.assert(vbl.type.match(/Int(32|16|8)/) !== null, 'The count variable "' + vbl.name + '" must have an integer type but it has "' + vbl.type + '"');
          add(vbl, 'count');
          break;
      }
    }
  }
  
  // Secondly, classify *coordinate* variables by corresponding dimensions, and *data* variables
  // as the rest.
  for(let vbl of this._vars)
  {
    if(classified.has(vbl) === true)
      continue;
    if(this._dims.find(dim => dim.name === vbl.name) !== undefined)
      add(vbl, 'coordinate');
    else
      add(vbl, 'data');
  }
}

} );
