define(['netcdf/Attribute',
        'netcdf/Dimension',
        'netcdf/File',
        'netcdf/Reader',
        'netcdf/SyntaxError',
        'netcdf/TypedData',
        'netcdf/Variable',
        'netcdf/Writer'],
        (NetCdfAttribute,
         NetCdfDimension,
         NetCdfFile,
         NetCdfReader,
         NetCdfSyntaxError,
         NetCdfTypedData,
         NetCdfVariable,
         NetCdfWriter) =>
/** Object to export the public components of the library. */
({

Reader: NetCdfReader,
SyntaxError: NetCdfSyntaxError,
File: NetCdfFile,
Dimension: NetCdfDimension,
Attribute: NetCdfAttribute,
Variable: NetCdfVariable,
TypedData: NetCdfTypedData,
Writer: NetCdfWriter

}) );
