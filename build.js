{
  name: '_netcdf',
  out: 'netcdf.js',
  optimize: 'none',
  // We want an anonymous module so that the path of the *netcdf.js* file can be used in require
  // calls of web apps that use this library.
  onBuildWrite: (moduleName, path, contents) => moduleName !== '_netcdf' ?
    contents : contents.replace("define('_netcdf',[", "define([")
}
