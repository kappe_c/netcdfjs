# Abilities and Limitations

This pure client JavaScript library does not depend on Node.js or the netCDF C library.
It can read and write netCDF files in the classic format, as specified
[here](https://www.unidata.ucar.edu/software/netcdf/docs/file_format_specifications.html).
The 64 bit variant is not yet supported but could probably be implemented quite easily.
It is not intended to support the netCDF-4 format.
The focus is on reading and writing a whole file in one go, but the code is quite well prepared to
do this only on demand, e.g. for individual records.

# Usage

The library should be loaded using *requirejs*.
Then `netcdf.Reader.read` can be used to read the data from a netCDF file that is given as
an array buffer. The returned `netcdf.File` object has the following properties, which
are pretty much self-explanatory considering what can be stored in a netCDF file.
Details, currently, need to be looked up in the JSDoc documentation within the source code files.

- name
- numRecords
- dims
    - name
    - numValues
- atts
    - name
    - type
    - data
- vars
    - name
    - type
    - data/records
    - dimIndices
    - atts
    - numValuesPerRecord
    - numBytesPerRecord

Just a little more information is provided for convenience.
If the `type` of an attribute is "Uint8", the `data` can also be accessed as `string`
(JavaScript UTF-16 string constructed from the netCDF UTF-8 data).
The `vars` of a file are also divided into `staticVars` and `recordVars`, and variables have a
respective `isRecordVar` property (if this is false, `records` is undefined).
There is also a `netcdf.Writer.write` function to write a netCDF file to an array buffer.

The following is a minimal example showing how to read a file and print the content on the console.
For more extensive use of the library, you may want to have a look at
[ncdump online](https://bitbucket.org/kappe_c/ncdump) and
[NetCDF Editor](https://bitbucket.org/kappe_c/netcdf_editor).

## index.html

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script data-main="main" src="require.js"></script>
  </head>
  <body>
    <input id="fileInput" type="file">
  </body>
</html>
```

## main.js

```javascript
require(['netcdf'], (netcdf) =>
{
  const onArrBufRead = (arrBuf, fileName) =>
  {
    const ncReader = new netcdf.Reader(arrBuf, fileName);
    let ncFile;
    try { ncFile = ncReader.read(); }
    catch(e) {
      if(e instanceof netcdf.SyntaxError) {
        console.error(e.message);
        alert(e.message);
        return;
      }
      else throw e;
    }
    
    
    // Add your code below. Here, just a simple dump of the file content.
    
    console.log(ncFile.dims.map(dim =>
      dim.name + ':' + (dim.numValues || ncFile.numRecords)
    ).join(', '));
    
    console.log(ncFile.atts.map(att =>
      '- ' + att.name + ': ' + (att.string || att.data.join(', '))
    ).join('\n'));
    
    console.log(ncFile.vars.map(vbl =>
    {
      const domain = '(' + vbl.dimIndices.map(i => ncFile.dims[i].name).join(', ') + ')';
      return vbl.name + ': ' + domain + ' => ' + vbl.type
    }).join('\n'));
  };
  
  const fileInp = document.getElementById('fileInput');

  fileInp.oninput = () =>
  {
    const file = fileInp.files[0];
    const reader = new FileReader();
    reader.onload = () => onArrBufRead(reader.result, file.name);
    reader.readAsArrayBuffer(file);
  };
});
```

## CF Conventions

To a small extent, the library supports the user in interpreting the file content with respect to
the [CF (Climate and Forecast) Conventions and Metadata](http://cfconventions.org).
Each `NetCdfFile` has a `kind2vars` object that allows the user to conveniently access variables
of a certain kind. The string keys are mapped to a (possibly empty) array of variables.
Consistent with this, each variable has a `kind` property that holds the respective string.

- data: Any variable that cannot be classified otherwise becomes a *data variable*.
- coordinate: Any variable that cannot be classified otherwise becomes a *coordinate variable* if there is a dimension with the same name.
- auxiliary: An *auxiliary coordinate variable* is determined by the *coordinates* attribute another variable may have. This kind of variable is used to indirectly specify the concrete position of a data point when no rectilinear mesh is used, e.g. the variables lon(cell) and lat(cell) with  data(cell).
- boundary: Coordinate variables may have a *bounds* attribute whose value is the name of a *boundary variable*. If the coordinate variable lon(lon) stores the centers of 1D cells (intervals), the boundary variable lon_bnds(lon, bnds) could be used to store the end points of these intervals.
- measure: A *measure variable* is determined by the *cell_measures* attribute another variable may have. As of CF-1.7 a measure variable contains either the area or volume of cells.
- count: A *count* variable is needed for a [contiguous ragged array representation](http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#_contiguous_ragged_array_representation) of data on [discrete sampling geometries](http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#discrete-sampling-geometries). It is marked with a *sample_dimension* attribute whose value is the name of the dimension it counts.

The data of data variables is stored in typed arrays (as opposed to ordinary JavaScript arrays that
know only one number type: double). This is because these variables usually
constitute the main chunks of the file and it makes reading and writing them more efficient, and
if the type is anything but *double* (often it is *float*) it also helps saving memory.
The ordinary JavaScript arrays and typed arrays have a syntactically and semantically almost
identical interface, so often one does not need to handle them differently, but e.g. the `map`
method of typed arrays will always return an array of the same type, i.e. mapping to strings is
not possible directly.


# Technical Information

This project contains the following files and directories.

- netcdf - JavaScript source code directory.
- _netcdf.js - Script used to export the public components of the library.
- build.js Script used by *r.js*.
- license.txt - Text of the LGPL which governs this project.
- package.json - Package configuration.
- r.js Script to compile *_netcdf.js* using *build.js*.
- readme.md - This readme file.

To build the library, run:
```
node r.js -o build.js
```

This build command creates an optimized version of the project in the file *netcdf.js*
(in the same directory as this readme file) which will be optimized to include all of its 
dependencies.

For more information on the optimizer:
[requirejs.org/docs/optimization.html](https://requirejs.org/docs/optimization.html)  
For more information on using requirejs:
[requirejs.org/docs/api.html](https://requirejs.org/docs/api.html)

The compiled library can also be found on
[Bitbucket](https://bitbucket.org/kappe_c/netcdfjs/downloads) under *Downloads*.
